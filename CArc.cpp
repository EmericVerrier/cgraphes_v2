#include "CArc.h"


CArc::CArc() {
}

CArc::CArc(unsigned int uiDestination) {
	uiARCDestination = uiDestination;
}


void CArc::ARCmodifyDestination(unsigned int uiDestination) {
	uiARCDestination = uiDestination;
}


unsigned int CArc::ARCreadDestination() {
	return uiARCDestination;
}